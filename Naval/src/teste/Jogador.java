package teste;

import java.util.Scanner;

/* Esta classe ira criar o jogador, seja ele uma pessoa ou o computador.
 * Nela sera definido a quantidade de naves e o tamanho delas,
 * assim como a definicao da posicao delas no tabuleiro.
 * Nela tambem sera definida a acao de atacar do jogador.
 */

public class Jogador {
	Scanner sc = new Scanner(System.in);

	// Variaveis
	private Integer qntNavesVivas; // Quando este valor for igual a 0 o jogo deve acabar
	private Nave[] naves;
	public Tabuleiro tabJog;
	public Tabuleiro tabJogParaIni;
	public Tabuleiro tabIni;

	// Constantes
	private static final int[] TAMANHO_NAVES = { 1, 2, 3 };
	private static final int NUM_NAVES = TAMANHO_NAVES.length;

	public Jogador() {
		// Inicializando as naves
		naves = new Nave[NUM_NAVES];
		for (int i = 0; i < NUM_NAVES; i++) {
			Nave nave = new Nave(TAMANHO_NAVES[i]);
			naves[i] = nave;
		}

		qntNavesVivas = 7; // Este valor e o total de posicoes que possuem naves

		// Inicializando tabuleiros
		tabJog = new Tabuleiro();
		tabJogParaIni = new Tabuleiro();
		tabIni = new Tabuleiro();

	}

	// Posicionar naves
	public void posNaves() {
		for (int a = 0; a < naves.length;) {
			Nave nave = naves[a];

			int tamanho = nave.getTamanho();
			int coluna;
			String direcaoSt;
			String linhaSt;

			tabJog.imprimirTab();

			// Coluna
			for (int j = 0; j < 1;) {
				System.out.println("Posicionando " + (a+1) + "� nave. Tamanho - " + nave.getTamanho());
				System.out.println("Informe a coluna - 0, 1, 2, 3, 4");
				coluna = sc.nextInt();
				if (coluna == 0 || coluna == 1 || coluna == 2 || coluna == 3 || coluna == 4) {
					nave.setColuna(coluna);
					j++;
				} else {
					System.out.println("Coluna invalida, informe valores entre 0 e 4");
				}
			}

			sc.nextLine(); // BUG

			// Linha
			for (int i = 0; i < 1;) {
				System.out.println("Informe a linha - A, B, C, D, E");
				linhaSt = sc.nextLine();
				linhaSt = linhaSt.toUpperCase();

				switch (linhaSt) {
				case "A":
					nave.setLinha(0);
					i++;
					break;

				case "B":
					nave.setLinha(1);
					i++;
					break;

				case "C":
					nave.setLinha(2);
					i++;
					break;

				case "D":
					nave.setLinha(3);
					i++;
					break;

				case "E":
					nave.setLinha(4);
					i++;
					break;

				default:
					System.out.println("Linha invalida, informe somente - A, B, C, D, E");
					break;
				}
			}

			// Direcao
			for (int i = 0; i < 1;) {
				System.out.println("Informe a direcao - Horizontal (H) ou  Vertical (V)");
				direcaoSt = sc.nextLine();
				direcaoSt = direcaoSt.toUpperCase();

				switch (direcaoSt) { 
				case "H":
					nave.setDirecao(0);
					if (tabJog.verificarDisponibilidade(nave.getLinha(), nave.getColuna(), nave.getTamanho(),
							nave.getDirecao()) == false) {
						for (int j = 0; j < tamanho; j++) {
							tabJog.addNave(nave.getLinha(), (nave.getColuna() + j));
							tabJogParaIni.addNaveEscondida(nave.getLinha(), (nave.getColuna() + j));
						}
						i++;
						a++;
					} else {
						System.out.println("Posicionamento de nave incomptivel");
						i++;
					}
					break;

				case "V":
					nave.setDirecao(1);
					if (tabJog.verificarDisponibilidade(nave.getLinha(), nave.getColuna(), nave.getTamanho(),
							nave.getDirecao()) == false) {
						for (int j = 0; j < tamanho; j++) {
							tabJog.addNave((nave.getLinha() + j), nave.getColuna());
							tabJogParaIni.addNaveEscondida((nave.getLinha() + j), nave.getColuna());
						}
						i++;
						a++;
					} else {
						System.out.println("Posicionamento de nave incomptivel");
						i++;
					}
					break;

				default:
					System.out.println("Direcao invalida, informe somente - H ou V");
					break;
				}

			}

			// TODO Verificar se a nave ultrapassa o tamanho do tabuleiro

		}

	}
	
	
	// APAGAR!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
	public void teste() {
		System.out.println("tab Jog");
		tabJog.imprimirTab();
		System.out.println("\ntab Ini");
		tabIni.imprimirTab();
	}
	
	// Atacar
	public void atacar() {
		for (int a = 0; a < 1;) {

			int coluna = -1;
			int linha;
			String linhaSt;

			tabIni.imprimirTab();

			// Coluna
			for (int j = 0; j < 1;) {
				System.out.println("Hora do ataque!");
				System.out.println("Informe a coluna - 0, 1, 2, 3, 4");
				coluna = sc.nextInt();
				if (coluna == 0 || coluna == 1 || coluna == 2 || coluna == 3 || coluna == 4) {
					j++;
				} else {
					System.out.println("Coluna invalida, informe valores entre 0 e 4");
				}
			}

			sc.nextLine(); // BUG

			// Linha
			for (int i = 0; i < 1;) {
				System.out.println("Informe a linha - A, B, C, D, E");
				linhaSt = sc.nextLine();
				linhaSt = linhaSt.toUpperCase();

				switch (linhaSt) {
				case "A":
					linha = 0;
					if(tabIni.atacar(linha, coluna) == true) {
						qntNavesVivas--;
					}
					i++;
					a++;
					break;

				case "B":
					linha = 1;
					if(tabIni.atacar(linha, coluna) == true) {
						qntNavesVivas--;
					}
					i++;
					a++;
					break;

				case "C":
					linha = 2;
					if(tabIni.atacar(linha, coluna) == true) {
						qntNavesVivas--;
					}
					i++;
					a++;
					break;

				case "D":
					linha = 3;
					if(tabIni.atacar(linha, coluna) == true) {
						qntNavesVivas--;
					}
					i++;
					a++;
					break;

				case "E":
					linha = 4;
					if(tabIni.atacar(linha, coluna) == true) {
						qntNavesVivas--;
					}
					i++;
					a++;
					break;

				default:
					System.out.println("Linha invalida, informe somente - A, B, C, D, E");
					break;
				}
			}
			
			if(qntNavesVivas == 0) { // Terminar jogo
				System.out.println("PARABENS!!! Voce derrotou todas as naves inimigas!!!!");
				System.exit(0);
			}

		}
	}


}
