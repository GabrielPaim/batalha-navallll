package teste;

/* Esta classe deve criar os modelos de naves disponiveis no jogo,
 * assim como definir os seus repectivos tamanhos, vida, posicao e direcao.
 * Estes valores sao recebidos na inicializacao da nave.
 */

public class Nave {
	// Variaveis
	private Integer linha;
	private Integer coluna;
	private Integer tamanho;
	private Integer direcao;
	private Integer vida;

	// Constantes de direcao da nave
	public static final int INDEFINIDO = -1;
	public static final int HORIZONTAL = 0;
	public static final int VERTICAL = 1;

	public Nave(int tamanho) {
		this.tamanho = tamanho;
		this.vida = tamanho;
		this.linha = INDEFINIDO;
		this.coluna = INDEFINIDO;
		this.direcao = INDEFINIDO;
	}
	
	// Getters and Setters
	public Integer getLinha() {
		return linha;
	}

	public void setLinha(Integer linha) {
		this.linha = linha;
	}

	public Integer getColuna() {
		return coluna;
	}

	public void setColuna(Integer coluna) {
		this.coluna = coluna;
	}

	public Integer getTamanho() {
		return tamanho;
	}

	public void setTamanho(Integer tamanho) {
		this.tamanho = tamanho;
	}

	public Integer getDirecao() {
		return direcao;
	}

	public void setDirecao(Integer direcao) {
		this.direcao = direcao;
	}

	public Integer getVida() {
		return vida;
	}

	public void setVida(Integer vida) {
		this.vida = vida;
	}
	
	

}
