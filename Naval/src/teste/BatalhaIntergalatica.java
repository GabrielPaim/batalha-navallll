package teste;

import java.util.Scanner;

/* Esta e a classe que possui o main.
 * Nesta classe deve possuir os metodos de imprimir tabuleiro,
 * assim como a inicializacao de todas as classes.
 */

public class BatalhaIntergalatica {
	static Scanner sc = new Scanner(System.in);
	
	public static void main(String[] args) {
		msgIntrodutoria();
		escolherModoJogo();

	}

	// Mensagem introdutoria
	private static void msgIntrodutoria() {
		System.out.println("....Em uma galaxia muito muito distante....");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("..........BATALHA INTERGALATICA............");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		
	}
	
	// Mensagem de inicio de batalha
	private static void msgInicioBatalha() {
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........QUE COMECE A BATALHA............");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
	}
	
	// Mensagem para espacamento entre jogadores
	private static void msgDeEspacamento() {
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
		System.out.println("...........................................");
	
	}

	// Escolher o modo de jogo
	private static void escolherModoJogo() {
		int modoJogo;
		for (int i = 0; i < 1;) {
			System.out.println("Escolha o modo de jogo");
			System.out.println("Digite 1 para jogar contra o compuador");
			System.out.println("Digite 2 para jogar contra outro jogador");
			modoJogo = sc.nextInt();
			sc.nextLine();
			
			switch (modoJogo) {
			case 1:
				posNaves1Jog();
				i++;
				break;
			case 2:
				posNaves2Jog();
				i++;
				break;
				
			default:
				System.out.println("Informe somente 1 ou 2");
				break;
			}
		}
	}
	
	// Posicionar naves no modo 2 jogadores
	public static void posNaves2Jog() {
		System.out.println();
		System.out.println("Modo 2 jogadores");
		
		// Posicionando as naves do 1 jogador
		System.out.println("Jogador 1");
		Jogador jog1 = new Jogador();
		jog1.posNaves();
		System.out.println();
		msgDeEspacamento();
		
		// Posicionando as naves do 2 jogador
		System.out.println("Jogador 2");
		Jogador jog2 = new Jogador();
		jog2.posNaves();
		
		// Definindo o tabuleiro inimigo
		jog1.tabIni = jog2.tabJogParaIni;
		jog2.tabIni = jog1.tabJogParaIni;
		
		
		// APAGAR
		System.out.println("Jog1");
		jog1.teste();
		System.out.println("jog2");
		jog2.teste();
		
		
		batalhar2Jog(jog1, jog2);

	}
	
	// Posicionar naves no modo 1 jogador
	public static void posNaves1Jog() {
			System.out.println();
			System.out.println("Modo 1 jogador");
			System.out.println("Jogador 1");
			Jogador jog1 = new Jogador();
			jog1.posNaves();
			//TODO posicionamento de nave do computador

	}
		
	// Batalhar no modo 1 jogador	
	public static void batalhar1Jog(Jogador jog1, Jogador jog2) {
		//TODO batalha utilizando a classe computador
	}
	
	// Batalhar no modo 2 jogadores
	public static void batalhar2Jog(Jogador jog1, Jogador jog2) {
		msgInicioBatalha();
		
		for (int i = 0; i < 1;) {
			System.out.println("Jogador 1");
			jog1.atacar();
			msgDeEspacamento();
			System.out.println("Jogador 2");
			jog2.atacar();
			msgDeEspacamento();
			
		}
	}

}
