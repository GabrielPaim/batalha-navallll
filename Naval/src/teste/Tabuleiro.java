package teste;

/* Esta classe ira criar o tabuleiro, ele e composto por 
 * um vetor bidimensional construido a partir da classe Posicao
 * Nela sera definido o tamanho do tabuleiro.
 */

public class Tabuleiro {
	// Variaveis
	private Posicao[][] tabuleiro;

	// Constantes
	public static final int NUM_LINHAS = 5; // A, B, C, D, E
	public static final int NUM_COLUNAS = 5; // 0, 1, 2, 3, 4
	private static final String[] linhas = { "A", "B", "C", "D", "E" };

	public Tabuleiro() {
		tabuleiro = new Posicao[NUM_LINHAS][NUM_COLUNAS]; // Inicializando o tabuleiro

		/* Preenchendo o tabuleiro com posicoes, estas posicoes, assim como definido no
		 * construtor da classe Posicao, no momento nao possuem nave e todas estao
		 * ESCONDIDA
		 */
		for (int linha = 0; linha < tabuleiro.length; linha++) {
			for (int coluna = 0; coluna < tabuleiro.length; coluna++) {
				Posicao posicao = new Posicao();
				tabuleiro[linha][coluna] = posicao;
			}
		}

	}
	

	public void imprimirTab() {
		System.out.println();

		// Espacamento para alinhas o tabuleiro
		System.out.print("  ");

		// Imprimir colunas
		for (int i = 0; i < NUM_COLUNAS; i++) {
			System.out.print(i + " ");
		}
		System.out.println();

		// Imprimir linhas
		for (int i = 0; i < NUM_LINHAS; i++) {
			System.out.print(linhas[i]);
			for (int j = 0; j < NUM_LINHAS; j++) {
				System.out.print(" " + tabuleiro[i][j].getRepresentacao());
			}
			System.out.println();
		}

	}

	// Adicionar nave
	public void addNave(int linha, int coluna) {
		Posicao posicao = tabuleiro[linha][coluna];
		posicao.addNave();

	}
	
	// Adicionar nava escondida
	public void addNaveEscondida(int linha, int coluna) {
		Posicao posicao = tabuleiro[linha][coluna];
		posicao.addNaveescondida();
	}
	
	// Atacar
	public boolean atacar(int linha, int coluna) {
		Posicao posicao = tabuleiro[linha][coluna];
		if(posicao.isPossuiNave() == true) {
			posicao.ataqueCerto();
			return true;
		} else {
			posicao.ataqueErrado();
			return false;
		}
	}
	
	// Verificar se ha nave na possicao
	public boolean verificarPossuiNave(int linha, int coluna) {
		Posicao posicao = tabuleiro[linha][coluna];
		if (posicao.isPossuiNave() == true) {
			return true;
		}
		return false;
	}

	// Verificar a disponibilidade das posicoes no qual a nave vai ser posicionada
	public boolean verificarDisponibilidade(int linha, int coluna, int tamanho, int direcao) { // direcao 0-H, 1-V
		int a = 0;

		if (tamanho == 0) {
			return verificarPossuiNave(linha, coluna);
		}

		if (direcao == 0) {
			int tamFinal = linha + tamanho - 1;
			if (tamFinal > 5) {
				return true;
			}

			for (int i = linha; i < tamFinal; i++) {
				if (verificarPossuiNave(i, coluna) == true) {
					a++;
				}
				if (a > 0) {
					return true;
				}

			}
		}

		if (direcao == 1) {
			int tamFinal = coluna + tamanho - 1;
			if (tamFinal > 5) {
				return true;
			}

			for (int i = coluna; i < tamFinal; i++) {
				if (verificarPossuiNave(linha, i) == true) {
					a++;
				}
				if (a > 0) {
					return true;
				}
			}
		}

		return false;
	}

}
