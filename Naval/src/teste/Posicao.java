package teste;

/* Esta classe representa cada posicao do tabuleiro.
 * Ela deve mostrar se ha algo na posicao (nave ou vazio), 
 * junto com a sua respectiva representacao(VAZIO, NAVE ou NAVE_ATINGIDA),
 * caso possua uma nave ela deve retornar tambem o seu status(ESCONDIDA ou ATINGIDA),
 * e caso nao possua uma nave ela deve retornar tambem o seu status(ESCONDIDA ou ERRADA)
 */

public class Posicao {
	// Variaveis
	private boolean possuiNave;
	private Integer status; // Utilizada junto as constantes para o status
	private String representacao; // Utilizada junto as constantes para a representacao

	// Constantes para o status
	public static final int ESCONDIDA = -1;
	public static final int ATINGIDA = 0;
	public static final int ERRADA = 1;

	// Contantes para a representacao
	public static final String VAZIO = "~";
	public static final String NAVE = "#";
	public static final String NAVE_ATINGIDA = "X";
	public static final String TIRO_ERRADO = "O";

	public Posicao() {
		// Inicializando valores
		status = ESCONDIDA;
		representacao = VAZIO;
		possuiNave = false;
	}
	
	// Adicionar nave a posicao
	public void addNave() {
		setRepresentacao(NAVE);
		setPossuiNave(true);
	}
	
	// Adicionar nave escondida
	public void addNaveescondida() {
		setRepresentacao(VAZIO);
		setPossuiNave(true);
	}
	
	// Ataque certo.
	public void ataqueCerto() {
		setStatus(ATINGIDA);
		setRepresentacao(NAVE_ATINGIDA);
		System.out.println("Acertou");
	}
	
	// Ataque errado.
	public void ataqueErrado() {
		setStatus(ERRADA);
		setRepresentacao(TIRO_ERRADO);
		System.out.println("Errou");
	}

	// Getters and Setters

	public boolean isPossuiNave() {
		return possuiNave;
	}

	public String getRepresentacao() {
		return representacao;
	}

	public void setRepresentacao(String representacao) {
		this.representacao = representacao;
	}

	public void setPossuiNave(boolean possuiNave) {
		this.possuiNave = possuiNave;
	}

	public Integer getStatus() {
		return status;
	}

	public void setStatus(Integer status) {
		this.status = status;
	}

}
